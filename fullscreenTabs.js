function changeTab(tab, action) {
    port.postMessage({
        tab: parseInt(tab),
        action: action
    });
}

function tabScroll(e) {
    var tabs = document.querySelector('#fullscreenTabs');
    var active = document.querySelector('.active');
    var tabId = 0;
    if (e.wheelDelta > 0) {
        if (active.previousSibling != null) {
            tabId = active.previousSibling.dataset.id;
        } else {
            tabId = tabs.children[tabs.children.length - 1].dataset.id;
        }
    } else if (e.wheelDelta < 0) {
        if (active.nextSibling != null) {
            tabId = active.nextSibling.dataset.id;
        } else {
            tabId = tabs.children[0].dataset.id;
        }
    }
    changeTab(tabId, 'activate');
    return false;
}

function updateTabs(data) {
    var tabs = document.querySelector('#fullscreenTabs');
    var closeTabButtons = document.querySelector("#closeTabButton")
    tabs.innerHTML = data.tabs;
    for (var i = 0; i < tabs.children.length; i++) {
        if (tabs.children[i].dataset.id == data.tabId) {
            tabs.children[i].className += ' active';
        }

        if(tabs.children[i] != null)
        {
            if(tabs.children[i].classList.contains("tab"))
            {
                tabs.children[i].onclick = function(e) {
                    changeTab(this.dataset.id, 'activate');
                };
            }
        }

        //Close the tab by taking the id number, minus the cb suffix and sending it to the change tab function
        var closeTabButton = document.getElementById(tabs.children[i].dataset.id + "cb");
        if (closeTabButton != null)
        {
            closeTabButton.onclick = function(e) {
                changeTab(this.id.replace("cb",""), 'close');
            };
        }

        tabs.onmousewheel = tabScroll;
    }
}

var port = chrome.runtime.connect({name: 'tabs'});
port.onMessage.addListener(updateTabs);
